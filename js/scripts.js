$(document).ready(function() {
    $('.header__button').click(function() {
        $('.header__navigation').slideToggle();
    });
});

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 1) {
        $(".header").addClass("header--sticky");
    } else {
        $(".header").removeClass("header--sticky");
    }
});